# Parkour manager

A plugin to manage checkpointed parkour courses, and speedruns of said courses.
Courses and runs are stored across servers in a MongoDB database.
See the project documentation and ingame help for detailed information about the plugin.

Note that this project uses a custom command parsing framework.
See the https://github.com/LlewVallis/command-builder for more information.