package net.cubekrowd.parkourmanager;

import com.mongodb.client.model.Filters;
import com.mongodb.client.model.ReplaceOptions;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Set;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.logging.Level;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;

@Data
@NoArgsConstructor
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class Course {

    @EqualsAndHashCode.Include
    private String name;

    private String worldName;
    private String serverId = ParkourManagerPlugin.getServerId();

    private List<Checkpoint> checkpoints = new ArrayList<>();

    public Course(String name, String worldName) {
        this.name = name;
        this.worldName = worldName;
    }

    public CompletableFuture<Void> save() {
        Set<Course> courses = ParkourManagerPlugin.getInstance().getCourses();
        // Force any properties to be updated
        courses.remove(this);
        courses.add(this);

        // Rebuild cached map of blocks to checkpoints
        CheckpointListener.invalidateCheckpointLocations();

        var collection = ParkourManagerPlugin.getInstance().getMongoCollection("courses", Course.class);
        return ListSubscriber.subscribeWithoutResult(collection.replaceOne(
                Filters.and(Filters.eq("name", name), Filters.eq("serverId", serverId)),
                this,
                new ReplaceOptions().upsert(true)
        ));
    }

    public CompletableFuture<Void> remove() {
        ParkourManagerPlugin.getInstance().getCourses().remove(this);

        // Rebuild cached map of blocks to checkpoints
        CheckpointListener.invalidateCheckpointLocations();

        PartialRun.clearByCourse(this).whenComplete((nothing, error) -> {
            if (error != null) {
                ParkourManagerPlugin.getLog().log(Level.SEVERE, "Failed to clear partial runs for " + name + " during deletion");
            }
        });

        var collection = ParkourManagerPlugin.getInstance().getMongoCollection("courses", Course.class);
        return ListSubscriber.subscribeWithoutResult(collection.deleteOne(Filters.eq("name", name)));
    }

    @SneakyThrows({ InterruptedException.class, ExecutionException.class})
    public static Course copyFromServer(String originalName, String otherServerId, String newName, String newWorld) {
        var collection = ParkourManagerPlugin.getInstance().getMongoCollection("courses", Course.class);

        List<Course> results = ListSubscriber.subscribeAndAwait(collection.find(
                Filters.and(Filters.eq("name", originalName), Filters.eq("serverId", otherServerId))
        ));

        if (results.size() == 0) {
            throw new NoSuchElementException("course " + originalName + " not found on server " + otherServerId);
        }

        Course course = results.get(0);
        course.setServerId(ParkourManagerPlugin.getServerId());
        course.setName(newName);
        course.setWorldName(newWorld);

        course.save().get();
        return course;
    }

    public static Course byName(String name) {
        return ParkourManagerPlugin.getInstance().getCourses().stream()
                .filter(course -> course.getName().equals(name))
                .findAny()
                .orElseThrow();
    }

    public static boolean exists(String name) {
        return ParkourManagerPlugin.getInstance().getCourses().stream()
                .anyMatch(course -> course.getName().equals(name));
    }

    public static Set<Course> loadFromDatabase() {
        var collection = ParkourManagerPlugin.getInstance().getMongoCollection("courses", Course.class);

        List<Course> courses = ListSubscriber.subscribeAndAwait(collection.find().filter(
                Filters.eq("serverId", ParkourManagerPlugin.getServerId())
        ));

        return new HashSet<>(courses);
    }
}
