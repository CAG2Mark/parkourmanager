package net.cubekrowd.parkourmanager;

import com.gmail.filoghost.holographicdisplays.api.Hologram;
import com.gmail.filoghost.holographicdisplays.api.HologramsAPI;
import org.bson.types.ObjectId;
import org.bukkit.Bukkit;
import org.bukkit.Location;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class HolographicDisplaysWrapper {

    private static HolographicDisplaysWrapper instance = null;

    private Map<ObjectId, Hologram> trackedHolos = new HashMap<>();

    public static HolographicDisplaysWrapper getInstance() {
        if (instance != null) {
            return instance;
        }

        if (Bukkit.getPluginManager().isPluginEnabled("HolographicDisplays")) {
            instance = new HolographicDisplaysWrapper();
            return instance;
        }

        return null;
    }

    public static void renderHolo(ObjectId key, Location location, List<String> lines) {
        if (getInstance() != null) {
            getInstance().render(key, location, lines);
        }
    }

    private void render(ObjectId key, Location location, List<String> lines) {
        Hologram hologram = trackedHolos.computeIfAbsent(key, _key ->
                HologramsAPI.createHologram(ParkourManagerPlugin.getInstance(), location));

        hologram.clearLines();
        lines.forEach(hologram::appendTextLine);
    }

    public static void removeHolo(ObjectId key) {
        if (getInstance() != null) {
            getInstance().remove(key);
        }
    }

    private void remove(ObjectId key) {
        Hologram hologram = trackedHolos.remove(key);
        if (hologram == null) {
            ParkourManagerPlugin.getInstance().getLogger().warning("Attempting to delete untracked holograph");
            return;
        }

        hologram.delete();
    }
}
