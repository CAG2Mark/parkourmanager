package net.cubekrowd.parkourmanager;

import org.bukkit.Bukkit;
import org.bukkit.boss.BarColor;
import org.bukkit.boss.BarStyle;
import org.bukkit.boss.BossBar;
import org.bukkit.entity.Player;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class BossBarPool {

    private static final Map<Player, BossBar> bossBars = new HashMap<>();

    public static BossBar create(Player player) {
        removeIfPresent(player);

        BossBar bossBar = Bukkit.createBossBar("Parkour progress", BarColor.GREEN, BarStyle.SOLID);
        bossBar.setProgress(0);
        bossBar.addPlayer(player);

        bossBars.put(player, bossBar);
        return bossBar;
    }

    public static void removeAll() {
        // Clone to avoid concurrent modification
        Set<Player> players = new HashSet<>(bossBars.keySet());
        players.forEach(BossBarPool::removeIfPresent);
    }

    public static void removeAfter(Player player, BossBar expectedBossBar, int ticks) {
        if (ticks == 0) {
            BossBar actualBossBar = bossBars.get(player);
            if (actualBossBar == expectedBossBar) {
                removeIfPresent(player);
            }
        } else {
            Bukkit.getScheduler().scheduleSyncDelayedTask(ParkourManagerPlugin.getInstance(),
                    () -> removeAfter(player, expectedBossBar, 0), ticks);
        }
    }

    private static void removeIfPresent(Player player) {
        if (bossBars.containsKey(player)) {
            bossBars.remove(player).removeAll();
        }
    }
}
