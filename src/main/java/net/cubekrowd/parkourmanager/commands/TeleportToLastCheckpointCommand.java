package net.cubekrowd.parkourmanager.commands;

import io.github.llewvallis.commandbuilder.*;
import net.cubekrowd.parkourmanager.Run;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.ComponentBuilder;
import org.bukkit.entity.Player;

@AutoSubCommand(ParkourCommandGroup.class)
public class TeleportToLastCheckpointCommand extends SubCommand {

    @Override
    public String getName() {
        return "last-checkpoint";
    }

    @Override
    public String getDescription() {
        return "Teleport to the last checkpoint you passed. If you haven't passed a checkpoint yet, you will be " +
                "teleported back to the start of the course and your time will be reset";
    }

    @Override
    public String getUsageMessage() {
        return "last-checkpoint";
    }

    @PlayerOnlyCommand
    @ExecuteCommand
    private void execute(CommandContext ctx) {
        Player player = (Player) ctx.getSender();

        Run runNullable = Run.getOrNull(player);
        if (runNullable == null) {
            ComponentBuilder message = new ComponentBuilder("You aren't on a parkour course").color(ChatColor.RED);
            player.spigot().sendMessage(message.create());
            return;
        }

        runNullable.teleportToLastCheckpoint();
    }
}
