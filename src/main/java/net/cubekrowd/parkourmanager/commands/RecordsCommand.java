package net.cubekrowd.parkourmanager.commands;

import io.github.llewvallis.commandbuilder.*;
import io.github.llewvallis.commandbuilder.arguments.StringSetArgument;
import net.cubekrowd.parkourmanager.CompletedRun;
import net.cubekrowd.parkourmanager.Course;
import net.cubekrowd.parkourmanager.ParkourManagerPlugin;
import net.cubekrowd.parkourmanager.Run;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.ComponentBuilder;
import org.bukkit.entity.Player;

import java.util.logging.Level;

@AutoSubCommand(ParkourCommandGroup.class)
public class RecordsCommand extends SubCommand {

    @Override
    public String getName() {
        return "records";
    }

    @PlayerOnlyCommand
    @ExecuteCommand
    private void execute(
            CommandContext ctx,
            Course course,
            @StringSetArgument.Arg({"weekly"}) @OptionalArg String weeklyString
    ) {
        Player player = (Player) ctx.getSender();
        boolean weekly = weeklyString != null;

        CompletedRun.fetchRuns(course, player.getUniqueId(), true, false, weekly, 0, 5).whenComplete((runs, error) -> {
            if (error == null) {
                int runCount = runs.size();
                ComponentBuilder builder;

                if (runCount > 0) {
                    builder = new ComponentBuilder("Your top " + runCount + " runs:").color(ChatColor.AQUA);

                    for (int i = 0; i < runCount; i++) {
                        CompletedRun run = runs.get(i);

                        builder.append("\n" + (i + 1) + ". ").color(ChatColor.GRAY);
                        builder.append(Run.getLongDurationString(run.getDuration())).color(ChatColor.AQUA);
                    }
                } else {
                    builder = new ComponentBuilder("You have no runs yet").color(ChatColor.RED);
                }

                player.spigot().sendMessage(builder.create());
            } else {
                ParkourManagerPlugin.getLog().log(Level.SEVERE, "Failed to get records for " + player + " on " + course);
                ComponentBuilder message = new ComponentBuilder("There was an error looking up your records, please " +
                        "contact a developer if the problem persists").color(ChatColor.RED);
                player.spigot().sendMessage(message.create());
            }
        });
    }
}
