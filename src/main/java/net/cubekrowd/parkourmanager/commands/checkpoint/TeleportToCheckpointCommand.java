package net.cubekrowd.parkourmanager.commands.checkpoint;

import io.github.llewvallis.commandbuilder.*;
import net.cubekrowd.parkourmanager.Checkpoint;
import net.cubekrowd.parkourmanager.Course;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import org.bukkit.entity.Player;

import java.util.List;

@AutoSubCommand(CheckpointCommandGroup.class)
public class TeleportToCheckpointCommand extends SubCommand {

    @Override
    public String getName() {
        return "teleport";
    }

    @PlayerOnlyCommand
    @ExecuteCommand
    private void execute(CommandContext ctx, Course course, int position) {
        List<Checkpoint> checkpoints = course.getCheckpoints();
        // Migrate from 1 to 0 indexing
        position -= 1;

        Checkpoint checkpoint;
        try {
            checkpoint = checkpoints.get(position);
        } catch (IndexOutOfBoundsException e) {
            BaseComponent[] message = new ComponentBuilder("No checkpoint at position " + (position + 1)).color(ChatColor.RED).create();
            ctx.getSender().spigot().sendMessage(message);
            return;
        }

        Player player = (Player) ctx.getSender();
        Checkpoint.teleport(player, course, checkpoint);
    }
}
