package net.cubekrowd.parkourmanager.commands.checkpoint;

import io.github.llewvallis.commandbuilder.AutoSubCommand;
import io.github.llewvallis.commandbuilder.CommandContext;
import io.github.llewvallis.commandbuilder.ExecuteCommand;
import io.github.llewvallis.commandbuilder.SubCommand;
import net.cubekrowd.parkourmanager.Checkpoint;
import net.cubekrowd.parkourmanager.Course;
import net.cubekrowd.parkourmanager.ParkourManagerPlugin;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.ComponentBuilder;

import java.util.List;
import java.util.logging.Level;

@AutoSubCommand(CheckpointCommandGroup.class)
public class DeleteCheckpointCommand extends SubCommand {

    @Override
    public String getName() {
        return "delete";
    }

    @ExecuteCommand
    private void execute(CommandContext ctx, Course course, int position) {
        List<Checkpoint> checkpoints = course.getCheckpoints();
        // Migrate from 1 to 0 indexing
        position -= 1;

        try {
            checkpoints.remove(position);
        } catch (IndexOutOfBoundsException e) {
            BaseComponent[] message = new ComponentBuilder("Cannot delete a checkpoint at position " + (position + 1)).color(ChatColor.RED).create();
            ctx.getSender().spigot().sendMessage(message);
            return;
        }

        course.save().whenComplete((nothing, error) -> {
            if (error == null) {
                BaseComponent[] message = new ComponentBuilder("Removed checkpoint").color(ChatColor.GREEN).create();
                ctx.getSender().spigot().sendMessage(message);
            } else {
                BaseComponent[] message = new ComponentBuilder("Failed to remove checkpoint from the database").color(ChatColor.RED).create();
                ctx.getSender().spigot().sendMessage(message);
                ParkourManagerPlugin.getLog().log(Level.SEVERE, "Failed to save course " + course.getName(), error);
            }
        });
    }
}
