package net.cubekrowd.parkourmanager.commands.checkpoint;

import io.github.llewvallis.commandbuilder.AutoSubCommand;
import io.github.llewvallis.commandbuilder.CommandContext;
import io.github.llewvallis.commandbuilder.ExecuteCommand;
import io.github.llewvallis.commandbuilder.SubCommand;
import net.cubekrowd.parkourmanager.Checkpoint;
import net.cubekrowd.parkourmanager.Course;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ComponentBuilder;

import java.util.List;

@AutoSubCommand(CheckpointCommandGroup.class)
public class ListCheckpointsCommand extends SubCommand {

    @Override
    public String getName() {
        return "list";
    }

    @ExecuteCommand
    private void execute(CommandContext ctx, Course course) {
        ComponentBuilder message = new ComponentBuilder("Checkpoints for " + course.getName() + " (click to tp):").color(ChatColor.AQUA);

        List<Checkpoint> checkpoints = course.getCheckpoints();
        for (int i = 0; i < checkpoints.size(); i++) {
            Checkpoint checkpoint = checkpoints.get(i);

            message.append("\n" + (i + 1) + ". ")
                    .color(ChatColor.GRAY)
                    .event(new ClickEvent(
                            ClickEvent.Action.RUN_COMMAND,
                            "/parkour checkpoint teleport " + course.getName() + " " + (i + 1)
                    ));

            message.append(checkpoint.toDisplayString()).color(ChatColor.AQUA);
        }

        ctx.getSender().spigot().sendMessage(message.create());
    }
}
