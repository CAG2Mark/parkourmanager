package net.cubekrowd.parkourmanager.commands.checkpoint;

import io.github.llewvallis.commandbuilder.*;
import io.github.llewvallis.commandbuilder.arguments.BlockCoordArgument;
import net.cubekrowd.parkourmanager.Checkpoint;
import net.cubekrowd.parkourmanager.Course;
import net.cubekrowd.parkourmanager.ParkourManagerPlugin;
import net.cubekrowd.parkourmanager.commands.ContinuousCoordinateArgument;
import net.cubekrowd.parkourmanager.commands.YawArgument;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.ComponentBuilder;

import java.util.List;
import java.util.logging.Level;

@AutoSubCommand(CheckpointCommandGroup.class)
public class CreateCheckpointCommand extends SubCommand {

    @Override
    public String getName() {
        return "create";
    }

    @ExecuteCommand
    private void execute(
            CommandContext ctx,
            Course course,
            @BlockCoordArgument.Arg(BlockCoordArgument.Axis.X) int x,
            @BlockCoordArgument.Arg(BlockCoordArgument.Axis.Y) int y,
            @BlockCoordArgument.Arg(BlockCoordArgument.Axis.Z) int z,
            @ContinuousCoordinateArgument.Arg(ContinuousCoordinateArgument.Axis.X) double xOffset,
            @ContinuousCoordinateArgument.Arg(ContinuousCoordinateArgument.Axis.Y) double yOffset,
            @ContinuousCoordinateArgument.Arg(ContinuousCoordinateArgument.Axis.Z) double zOffset,
            @ClassArg(YawArgument.class) int yaw,
            @OptionalArg Integer position
    ) {
        Checkpoint checkpoint = new Checkpoint(
                x, y, z,
                xOffset - x, yOffset - y, zOffset - z,
                Math.floorMod(yaw, 360)
        );

        List<Checkpoint> checkpoints = course.getCheckpoints();

        if (checkpoints.contains(checkpoint)) {
            BaseComponent[] message = new ComponentBuilder("A checkpoint already exists in that location").color(ChatColor.RED).create();
            ctx.getSender().spigot().sendMessage(message);
            return;
        }

        if (position == null) {
            position = checkpoints.size();
        } else {
            // Migrate from 1 to 0 indexing
            position -= 1;
        }

        try {
            checkpoints.add(position, checkpoint);
        } catch (IndexOutOfBoundsException e) {
            BaseComponent[] message = new ComponentBuilder("Cannot add a checkpoint at position " + (position + 1)).color(ChatColor.RED).create();
            ctx.getSender().spigot().sendMessage(message);
            return;
        }

        course.save().whenComplete((nothing, error) -> {
            if (error == null) {
                BaseComponent[] message = new ComponentBuilder("Added checkpoint").color(ChatColor.GREEN).create();
                ctx.getSender().spigot().sendMessage(message);
            } else {
                BaseComponent[] message = new ComponentBuilder("Failed to save checkpoint to the database").color(ChatColor.RED).create();
                ctx.getSender().spigot().sendMessage(message);
                ParkourManagerPlugin.getLog().log(Level.SEVERE, "Failed to save course " + course.getName(), error);
            }
        });
    }
}
