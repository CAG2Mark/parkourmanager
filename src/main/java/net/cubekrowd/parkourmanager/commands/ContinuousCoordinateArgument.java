package net.cubekrowd.parkourmanager.commands;

import io.github.llewvallis.commandbuilder.*;
import org.bukkit.Location;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Entity;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.util.*;

public class ContinuousCoordinateArgument implements ArgumentParser<Double> {

    private final Axis axis;

    public ContinuousCoordinateArgument(Axis axis) {
        this.axis = axis;
    }

    public enum Axis {
        X, Y, Z
    }

    @ParserAnnotation(ContinuousCoordinateArgument.class)
    @Retention(RetentionPolicy.RUNTIME)
    @Target(ElementType.PARAMETER)
    public @interface Arg {

        Axis value();
    }

    @ArgumentInferenceFactory
    private static ContinuousCoordinateArgument createParserFromAnnotation(ArgumentInferenceContext<Arg> ctx) {
        return new ContinuousCoordinateArgument(ctx.getAnnotation().value());
    }

    @Override
    public Double parse(String argument, int position, CommandContext context) throws ArgumentParseException {
        boolean relative = argument.startsWith("~");
        if (relative) {
            argument = argument.substring(1);
        }

        double value;
        if (relative && argument.equals("")) {
            value = 0;
        } else {
            try {
                value = Double.parseDouble(argument);
            } catch (NumberFormatException e) {
                throw new ArgumentParseException("invalid coordinate");
            }
        }

        if (relative) {
            value += getSenderCoord(context.getSender()).orElseThrow(() ->
                    new ArgumentParseException("cannot use relative coordinates in this context"));
        }

        return value;
    }

    @Override
    public Set<String> complete(List<Object> parsedArguments, String currentArgument, int position, CommandContext context) {
        return getSenderCoord(context.getSender())
                .map(dbl -> String.format(Locale.ENGLISH, "%.3f", dbl))
                .map(Set::of)
                .orElse(Collections.emptySet());
    }

    private Optional<Double> getSenderCoord(CommandSender sender) {
        if (sender instanceof Entity)  {
            Location location = ((Entity) sender).getLocation();

            switch (axis) {
                case X:
                    return Optional.of(location.getX());
                case Y:
                    return Optional.of(location.getY());
                case Z:
                    return Optional.of(location.getZ());
            }
        }

        return Optional.empty();
    }
}
