package net.cubekrowd.parkourmanager.commands;

import io.github.llewvallis.commandbuilder.AutoSubCommand;
import io.github.llewvallis.commandbuilder.CommandContext;
import io.github.llewvallis.commandbuilder.ExecuteCommand;
import io.github.llewvallis.commandbuilder.SubCommand;
import net.cubekrowd.parkourmanager.Checkpoint;
import net.cubekrowd.parkourmanager.Course;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.ComponentBuilder;
import org.bukkit.entity.Player;

@AutoSubCommand(ParkourCommandGroup.class)
public class TeleportToCourseCommand extends SubCommand {

    @Override
    public String getName() {
        return "play";
    }

    @Override
    public String getDescription() {
        return "Teleport to the start of a course, abandoning any course currently being run";
    }

    @Override
    public String getUsageMessage() {
        return "play <course>";
    }

    @ExecuteCommand
    private void execute(CommandContext ctx, Course course) {
        if (course.getCheckpoints().size() == 0) {
            ComponentBuilder message = new ComponentBuilder("That course has no checkpoints").color(ChatColor.RED);
            ctx.getSender().spigot().sendMessage(message.create());
            return;
        }

        Player player = (Player) ctx.getSender();
        Checkpoint checkpoint = course.getCheckpoints().get(0);
        Checkpoint.teleport(player, course, checkpoint);
    }
}
