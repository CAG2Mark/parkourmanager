package net.cubekrowd.parkourmanager.commands.course;

import io.github.llewvallis.commandbuilder.AutoSubCommand;
import io.github.llewvallis.commandbuilder.CommandContext;
import io.github.llewvallis.commandbuilder.ExecuteCommand;
import io.github.llewvallis.commandbuilder.SubCommand;
import net.cubekrowd.parkourmanager.Course;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.ComponentBuilder;
import org.bukkit.World;

import java.util.NoSuchElementException;

@AutoSubCommand(CourseCommandGroup.class)
public class CopyCourseCommand extends SubCommand {

    @Override
    public String getName() {
        return "copy";
    }

    @ExecuteCommand
    private void execute(CommandContext ctx, String originalName, String serverId, String newName, World newWorld) {
        try {
            Course.copyFromServer(originalName, serverId, newName, newWorld.getName());
        } catch (NoSuchElementException e) {
            ComponentBuilder message = new ComponentBuilder("Could not find " + originalName + " on " + serverId).color(ChatColor.RED);
            ctx.getSender().spigot().sendMessage(message.create());
            return;
        }

        ComponentBuilder message = new ComponentBuilder("Copied " + originalName + " from " + serverId).color(ChatColor.GREEN);
        ctx.getSender().spigot().sendMessage(message.create());
    }
}
