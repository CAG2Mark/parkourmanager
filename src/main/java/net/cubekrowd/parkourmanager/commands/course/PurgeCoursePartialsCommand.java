package net.cubekrowd.parkourmanager.commands.course;

import io.github.llewvallis.commandbuilder.AutoSubCommand;
import io.github.llewvallis.commandbuilder.CommandContext;
import io.github.llewvallis.commandbuilder.ExecuteCommand;
import io.github.llewvallis.commandbuilder.SubCommand;
import net.cubekrowd.parkourmanager.Course;
import net.cubekrowd.parkourmanager.ParkourManagerPlugin;
import net.cubekrowd.parkourmanager.PartialRun;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.ComponentBuilder;

import java.util.logging.Level;

@AutoSubCommand(CourseCommandGroup.class)
public class PurgeCoursePartialsCommand extends SubCommand {

    @Override
    public String getName() {
        return "purge-partials";
    }

    @ExecuteCommand
    private void execute(CommandContext ctx, Course course) {
        PartialRun.clearByCourse(course).handle((results, error) -> {
            if (error != null) {
                ParkourManagerPlugin.getLog().log(Level.SEVERE, "Failed to purge partials for " + course.getName(), error);

                ComponentBuilder message = new ComponentBuilder("Failed to purge partials for " + course.getName())
                        .color(ChatColor.RED);
                ctx.getSender().spigot().sendMessage(message.create());
            } else if (results.size() == 0) {
                // Note that results.size() == 0 means nothing happened, not that no elements were removed

                ParkourManagerPlugin.getLog().log(Level.SEVERE, "Failed to purge partials for " + course.getName() + ", no delete result");

                ComponentBuilder message = new ComponentBuilder("Failed to purge partials for " + course.getName()).color(ChatColor.RED);
                ctx.getSender().spigot().sendMessage(message.create());
            } else {
                ComponentBuilder message = new ComponentBuilder("Purged " + results.get(0).getDeletedCount() +
                        " partial(s) for " + course.getName())
                        .color(ChatColor.GREEN);
                ctx.getSender().spigot().sendMessage(message.create());
            }

            return null;
        });
    }
}
