package net.cubekrowd.parkourmanager.commands.course;

import io.github.llewvallis.commandbuilder.AutoSubCommand;
import io.github.llewvallis.commandbuilder.CommandContext;
import io.github.llewvallis.commandbuilder.ExecuteCommand;
import io.github.llewvallis.commandbuilder.SubCommand;
import net.cubekrowd.parkourmanager.Course;
import net.cubekrowd.parkourmanager.ParkourManagerPlugin;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ComponentBuilder;

import java.util.Set;

@AutoSubCommand(CourseCommandGroup.class)
public class ListCoursesCommand extends SubCommand {

    @Override
    public String getName() {
        return "list";
    }

    @ExecuteCommand
    private void execute(CommandContext ctx) {
        Set<Course> courses = ParkourManagerPlugin.getInstance().getCourses();

        ComponentBuilder builder = new ComponentBuilder("Courses for " + ParkourManagerPlugin.getServerId() + " (click for checkpoints):")
                .color(ChatColor.AQUA);

        for (Course course : courses) {
            builder.append("\n- ")
                    .color(ChatColor.GRAY)
                    .event(new ClickEvent(
                            ClickEvent.Action.RUN_COMMAND,
                            "/parkour checkpoint list " + course.getName()
                    ));

            builder.append(course.getName() + " (" + course.getWorldName() + ")").color(ChatColor.AQUA);
        }

        ctx.getSender().spigot().sendMessage(builder.create());
    }
}
