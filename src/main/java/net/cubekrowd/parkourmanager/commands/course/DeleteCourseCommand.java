package net.cubekrowd.parkourmanager.commands.course;

import io.github.llewvallis.commandbuilder.AutoSubCommand;
import io.github.llewvallis.commandbuilder.CommandContext;
import io.github.llewvallis.commandbuilder.ExecuteCommand;
import io.github.llewvallis.commandbuilder.SubCommand;
import net.cubekrowd.parkourmanager.Course;
import net.cubekrowd.parkourmanager.ParkourManagerPlugin;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.ComponentBuilder;

import java.util.logging.Level;

@AutoSubCommand(CourseCommandGroup.class)
public class DeleteCourseCommand extends SubCommand {

    @Override
    public String getName() {
        return "delete";
    }

    @ExecuteCommand
    private void execute(CommandContext ctx, Course course) {
        course.remove().whenComplete((nothing, error) -> {
            if (error == null) {
                BaseComponent[] message = new ComponentBuilder("Deleted " + course.getName()).color(ChatColor.GREEN).create();
                ctx.getSender().spigot().sendMessage(message);
            } else {
                BaseComponent[] message = new ComponentBuilder("Failed to delete " + course.getName() + " from the database").color(ChatColor.RED).create();
                ctx.getSender().spigot().sendMessage(message);
                ParkourManagerPlugin.getLog().log(Level.SEVERE, "Failed to delete course " + course.getName(), error);
            }
        });

        return;
    }
}
