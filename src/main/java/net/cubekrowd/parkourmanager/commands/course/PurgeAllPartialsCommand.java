package net.cubekrowd.parkourmanager.commands.course;

import io.github.llewvallis.commandbuilder.AutoSubCommand;
import io.github.llewvallis.commandbuilder.CommandContext;
import io.github.llewvallis.commandbuilder.ExecuteCommand;
import io.github.llewvallis.commandbuilder.SubCommand;
import net.cubekrowd.parkourmanager.ParkourManagerPlugin;
import net.cubekrowd.parkourmanager.PartialRun;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.ComponentBuilder;

import java.util.logging.Level;

@AutoSubCommand(CourseCommandGroup.class)
public class PurgeAllPartialsCommand extends SubCommand {

    @Override
    public String getName() {
        return "purge-all-partials";
    }

    @ExecuteCommand
    private void execute(CommandContext ctx) {
        PartialRun.clearAll().handle((results, error) -> {
            if (error != null) {
                ParkourManagerPlugin.getLog().log(Level.SEVERE, "Failed to purge all partials", error);

                ComponentBuilder message = new ComponentBuilder("Failed to purge partials").color(ChatColor.RED);
                ctx.getSender().spigot().sendMessage(message.create());
            } else if (results.size() == 0) {
                // Note that results.size() == 0 means nothing happened, not that no elements were removed

                ParkourManagerPlugin.getLog().log(Level.SEVERE, "Failed to purge partials, no delete result");

                ComponentBuilder message = new ComponentBuilder("Failed to purge partials").color(ChatColor.RED);
                ctx.getSender().spigot().sendMessage(message.create());
            } else {
                ComponentBuilder message = new ComponentBuilder("Purged " + results.get(0).getDeletedCount() + " partial(s)")
                        .color(ChatColor.GREEN);
                ctx.getSender().spigot().sendMessage(message.create());
            }

            return null;
        });
    }
}
