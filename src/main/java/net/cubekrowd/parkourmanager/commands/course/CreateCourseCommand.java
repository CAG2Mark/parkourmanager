package net.cubekrowd.parkourmanager.commands.course;

import io.github.llewvallis.commandbuilder.*;
import net.cubekrowd.parkourmanager.Course;
import net.cubekrowd.parkourmanager.ParkourManagerPlugin;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import org.bukkit.entity.Player;

import java.util.logging.Level;

@AutoSubCommand(CourseCommandGroup.class)
public class CreateCourseCommand extends SubCommand {

    @Override
    public String getName() {
        return "create";
    }

    @PlayerOnlyCommand
    @ExecuteCommand
    private void execute(CommandContext ctx, String name) {
        if (Course.exists(name)) {
            BaseComponent[] message = new ComponentBuilder("That course already exists").color(ChatColor.RED).create();
            ctx.getSender().spigot().sendMessage(message);
            return;
        }

        Course course = new Course(name, ((Player) ctx.getSender()).getWorld().getName());

        course.save().whenComplete((nothing, error) -> {
            if (error == null) {
                BaseComponent[] message = new ComponentBuilder("Created " + name).color(ChatColor.GREEN).create();
                ctx.getSender().spigot().sendMessage(message);
            } else {
                BaseComponent[] message = new ComponentBuilder("Failed to save " + name + " to the database").color(ChatColor.RED).create();
                ctx.getSender().spigot().sendMessage(message);
                ParkourManagerPlugin.getLog().log(Level.SEVERE, "Failed to save course " + course.getName(), error);
            }
        });
    }
}
