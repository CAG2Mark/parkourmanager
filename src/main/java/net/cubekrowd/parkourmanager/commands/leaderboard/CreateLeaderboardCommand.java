package net.cubekrowd.parkourmanager.commands.leaderboard;

import io.github.llewvallis.commandbuilder.*;
import io.github.llewvallis.commandbuilder.arguments.StringSetArgument;
import net.cubekrowd.parkourmanager.Course;
import net.cubekrowd.parkourmanager.Leaderboard;
import net.cubekrowd.parkourmanager.ParkourManagerPlugin;
import net.cubekrowd.parkourmanager.commands.ContinuousCoordinateArgument;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import org.bukkit.Location;
import org.bukkit.entity.Player;

import java.util.logging.Level;

@AutoSubCommand(LeaderboardCommandGroup.class)
public class CreateLeaderboardCommand extends SubCommand {

    @Override
    public String getName() {
        return "create";
    }

    @ExecuteCommand
    @PlayerOnlyCommand
    private void execute(
            CommandContext ctx,
            Course course,
            @ContinuousCoordinateArgument.Arg(ContinuousCoordinateArgument.Axis.X) double x,
            @ContinuousCoordinateArgument.Arg(ContinuousCoordinateArgument.Axis.Y) double y,
            @ContinuousCoordinateArgument.Arg(ContinuousCoordinateArgument.Axis.Z) double z,
            @StringSetArgument.Arg({"weekly"}) @OptionalArg String weeklyString
    ) {
        Leaderboard.warnIfInvisible(ctx.getSender());
        Player player = (Player) ctx.getSender();
        boolean weekly = weeklyString != null;

        Leaderboard.create(
                course,
                new Location(player.getWorld(), x, y, z),
                weekly
        ).whenComplete((leaderboard, error) -> {
            if (error == null) {
                BaseComponent[] message = new ComponentBuilder("Created leaderboard for " + course.getName())
                        .color(ChatColor.GREEN).create();
                ctx.getSender().spigot().sendMessage(message);
            } else {
                BaseComponent[] message = new ComponentBuilder("Failed to save leaderboard to the database")
                        .color(ChatColor.RED).create();
                ctx.getSender().spigot().sendMessage(message);
                ParkourManagerPlugin.getLog().log(Level.SEVERE, "Failed to save leaderboard for " + course.getName(), error);
            }
        });
    }
}
