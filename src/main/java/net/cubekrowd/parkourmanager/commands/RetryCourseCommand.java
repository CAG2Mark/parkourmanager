package net.cubekrowd.parkourmanager.commands;

import io.github.llewvallis.commandbuilder.*;
import net.cubekrowd.parkourmanager.Checkpoint;
import net.cubekrowd.parkourmanager.Course;
import net.cubekrowd.parkourmanager.Run;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.ComponentBuilder;
import org.bukkit.entity.Player;

@AutoSubCommand(ParkourCommandGroup.class)
public class RetryCourseCommand extends SubCommand {

    @Override
    public String getName() {
        return "retry";
    }

    @PlayerOnlyCommand
    @ExecuteCommand
    private void execute(CommandContext ctx) {
        Player player = (Player) ctx.getSender();

        Run runNullable = Run.getOrNull(player);
        if (runNullable == null) {
            ComponentBuilder message = new ComponentBuilder("You aren't on a parkour course").color(ChatColor.RED);
            player.spigot().sendMessage(message.create());
            return;
        }

        Course course = runNullable.getCourse();
        Checkpoint checkpoint = course.getCheckpoints().get(0);
        Checkpoint.teleport(player, course, checkpoint);
    }
}
