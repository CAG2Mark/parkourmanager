package net.cubekrowd.parkourmanager.commands;

import io.github.llewvallis.commandbuilder.ArgumentParseException;
import io.github.llewvallis.commandbuilder.ArgumentParser;
import io.github.llewvallis.commandbuilder.CommandContext;
import io.github.llewvallis.commandbuilder.arguments.IntegerArgument;
import org.bukkit.entity.Entity;

import java.util.List;
import java.util.Set;

public class YawArgument implements ArgumentParser<Integer> {

    private final IntegerArgument underlying = new IntegerArgument();

    @Override
    public Integer parse(String argument, int position, CommandContext context) throws ArgumentParseException {
        if (argument.equals("~")) {
            if (context.getSender() instanceof Entity) {
                return (int) ((Entity) context.getSender()).getLocation().getYaw();
            } else {
                throw new ArgumentParseException("cannot use relative yaw in this context");
            }
        }

        return underlying.parse(argument, position, context);
    }

    @Override
    public Set<String> complete(List<Object> parsedArguments, String currentArgument, int position, CommandContext context) {
        if (context.getSender() instanceof Entity) {
            float yaw = ((Entity) context.getSender()).getLocation().getYaw();
            yaw = Math.round(yaw / 15) * 15;
            return Set.of(Integer.toString((int) yaw));
        }

        return Set.of();
    }
}
