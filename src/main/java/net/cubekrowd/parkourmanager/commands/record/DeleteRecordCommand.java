package net.cubekrowd.parkourmanager.commands.record;

import io.github.llewvallis.commandbuilder.AutoSubCommand;
import io.github.llewvallis.commandbuilder.CommandContext;
import io.github.llewvallis.commandbuilder.ExecuteCommand;
import io.github.llewvallis.commandbuilder.SubCommand;
import net.cubekrowd.parkourmanager.CompletedRun;
import net.cubekrowd.parkourmanager.ParkourManagerPlugin;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.ComponentBuilder;
import org.bson.types.ObjectId;

import java.util.logging.Level;

@AutoSubCommand(RecordCommandGroup.class)
public class DeleteRecordCommand extends SubCommand {

    @Override
    public String getName() {
        return "delete";
    }

    @ExecuteCommand
    private void execute(CommandContext ctx, String id) {
        if (!ObjectId.isValid(id)) {
            ComponentBuilder message = new ComponentBuilder("Invalid ID").color(ChatColor.RED);
            ctx.getSender().spigot().sendMessage(message.create());
            return;
        }

        CompletedRun.deleteById(new ObjectId(id)).whenComplete((results, error) -> {
            if (error != null) {
                ParkourManagerPlugin.getLog().log(Level.SEVERE, "Failed to delete record " + id, error);

                ComponentBuilder message = new ComponentBuilder("Failed to delete record").color(ChatColor.RED);
                ctx.getSender().spigot().sendMessage(message.create());
            } else if (results.size() == 0) {
                // Note that results.size() == 0 means nothing happened, not that no elements were removed

                ParkourManagerPlugin.getLog().log(Level.SEVERE, "Failed to delete record " + id + ", no delete result");

                ComponentBuilder message = new ComponentBuilder("Failed to delete record").color(ChatColor.RED);
                ctx.getSender().spigot().sendMessage(message.create());
            } else if (results.get(0).getDeletedCount() == 0) {
                ComponentBuilder message = new ComponentBuilder("No record exists with ID " + id).color(ChatColor.RED);
                ctx.getSender().spigot().sendMessage(message.create());
            } else {
                ComponentBuilder message = new ComponentBuilder("Deleted record " + id).color(ChatColor.GREEN);
                ctx.getSender().spigot().sendMessage(message.create());
            }
        });
    }
}
