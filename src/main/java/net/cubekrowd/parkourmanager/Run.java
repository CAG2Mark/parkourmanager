package net.cubekrowd.parkourmanager;

import java.util.Comparator;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import lombok.Getter;
import net.cubekrowd.lobbygames.ClickableItem;
import net.cubekrowd.lobbygames.GameLeftEvent;
import net.cubekrowd.lobbygames.GameStartedEvent;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.ChatMessageType;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.TextComponent;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.boss.BossBar;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public class Run {
    public static float DEFAULT_WALK_SPEED = 0.2f;

    private final Player player;
    @Getter
    private final Course course;
    private final BossBar bossBar;

    private final Set<Checkpoint> completedCheckpoints = new HashSet<>();

    private long startTime;

    // Marker for whether the run is active, used to prevent time player updates from continuing into eternity
    private boolean alive = true;

    private int speedChangePreventionTask;
    private int elytraPreventionTask;
    private int ridingPreventionTask;

    public Run(Player player, Course course, long startTime) {
        this.player = player;
        this.course = course;
        this.startTime = startTime;

        bossBar = BossBarPool.create(player);
        completedCheckpoints.add(course.getCheckpoints().get(0));

        // @NOTE(traks) If a player changes their walk speed and then starts a
        // run, this will end the run. Previously we set the walk speed to the
        // default when a run is started, but players kept the momentum they
        // had, which gave them an ever so slight advantage.
        //
        // Another issue I realise now, is that players with high ping received
        // the walk speed change later, so they got a larger advantage.
        speedChangePreventionTask = Bukkit.getScheduler().scheduleSyncRepeatingTask(ParkourManagerPlugin.getInstance(), () -> {
            if (player.getWalkSpeed() != DEFAULT_WALK_SPEED) {
                Run.cancel(player);

                ComponentBuilder message = new ComponentBuilder("Your run was cancelled because you changed your speed")
                        .color(ChatColor.RED);
                player.spigot().sendMessage(message.create());

                sendResumeTipMessage();
            }
        }, 0, 1);

        // @NOTE(zorua) If a player somehow gets an elytra without using a command this will deal with it
        elytraPreventionTask = Bukkit.getScheduler().scheduleSyncRepeatingTask(ParkourManagerPlugin.getInstance(), () -> {
            ItemStack chestplate = player.getInventory().getChestplate();
            if (chestplate != null && chestplate.getType() == Material.ELYTRA) {
                Run.cancel(player);

                ComponentBuilder message = new ComponentBuilder("Your run was cancelled because you are wearing elytra")
                        .color(ChatColor.RED);
                player.spigot().sendMessage(message.create());

                sendResumeTipMessage();
            }
        }, 0, 1);
        
        // @NOTE(hraponssi) Block riding any mobs or players, riding a player with
        // elytra and rocket can be a big help.
        ridingPreventionTask = Bukkit.getScheduler().scheduleSyncRepeatingTask(ParkourManagerPlugin.getInstance(), () -> {
            if (player.isInsideVehicle()) {
                Run.cancel(player);

                ComponentBuilder message = new ComponentBuilder("Your run was cancelled because you are in a vehicle")
                        .color(ChatColor.RED);
                player.spigot().sendMessage(message.create());

                sendResumeTipMessage();
            }
        }, 0, 1);
    }

    public void onCheckpoint(Checkpoint checkpoint) {
        List<Checkpoint> allCourseCheckpoints = course.getCheckpoints();
        int checkpointIndex = allCourseCheckpoints.indexOf(checkpoint);

        if (!hasCompletedAllCheckpointsBefore(checkpointIndex)) {
            ComponentBuilder message = new ComponentBuilder("You skipped a checkpoint, go back for it!").color(ChatColor.RED);
            player.spigot().sendMessage(message.create());
            return;
        }

        boolean isFirstTimeCompletingCheckpoint = completedCheckpoints.add(checkpoint);
        boolean isLastCheckpoint = checkpointIndex == allCourseCheckpoints.size() - 1;

        float progress = (completedCheckpoints.size() - 1) / ((float) allCourseCheckpoints.size() - 1);
        bossBar.setProgress(progress);

        long duration = System.currentTimeMillis() - startTime;

        if (isLastCheckpoint) {
            onCourseComplete(duration);
        } else if (isFirstTimeCompletingCheckpoint) {
            onValidCheckpointComplete(checkpointIndex, duration);
        } else if (checkpointIndex == 0 && completedCheckpoints.size() == 1) {
            // If the player runs back over the first checkpoint and hasn't completed any others
            startTime = System.currentTimeMillis();
        }
    }

    public boolean hasReachedCheckpoint() {
        return completedCheckpoints.size() > 1;
    }

    private boolean hasCompletedAllCheckpointsBefore(int checkpointIndex) {
        List<Checkpoint> allCourseCheckpoints = course.getCheckpoints();

        for (int i = 0; i < checkpointIndex; i++) {
            if (!completedCheckpoints.contains(allCourseCheckpoints.get(i))) {
                return false;
            }
        }

        return true;
    }

    private void onCourseComplete(long duration) {
        ComponentBuilder message = new ComponentBuilder("You finished the course in ").bold(true).color(ChatColor.AQUA)
                .append(getLongDurationString(duration)).color(ChatColor.GREEN);
        player.spigot().sendMessage(message.create());

        // Don't save records unless configured to
        if (ParkourManagerPlugin.shouldSaveRecords()) {
            CompletedRun completedRun = new CompletedRun(player, course, duration, new Date());
            completedRun.save().whenComplete((nothing, error) -> {
                if (error != null) {
                    ParkourManagerPlugin.getLog().log(Level.SEVERE, "Failed to save run " + completedRun, error);

                    ComponentBuilder errorMessage = new ComponentBuilder("Your run could not be saved, please contact " +
                            "a developer if the problem persists").color(ChatColor.RED);
                    player.spigot().sendMessage(errorMessage.create());
                }
            });
        }

        PartialRun.clear(player, course).whenComplete((nothing, error) -> {
            if (error != null) {
                ParkourManagerPlugin.getLog().log(Level.SEVERE, "Failed to clear partial run", error);
            }
        });

        remove(true);
    }

    private void onValidCheckpointComplete(int checkpointIndex, long duration) {
        ComponentBuilder message = new ComponentBuilder("You reached checkpoint ").color(ChatColor.AQUA)
                .append("#" + checkpointIndex).color(ChatColor.GREEN)
                .append(" at ").color(ChatColor.AQUA)
                .append(getLongDurationString(duration)).color(ChatColor.GREEN);
        player.spigot().sendMessage(message.create());
    }

    public void teleportToLastCheckpoint() {
        List<Checkpoint> allCheckpoints = course.getCheckpoints();

        Checkpoint checkpoint = completedCheckpoints.stream()
                .max(Comparator.comparingInt(allCheckpoints::indexOf))
                .orElseThrow();

        // If teleporting to the first checkpoint, also reset the time
        if (allCheckpoints.indexOf(checkpoint) == 0) {
            startTime = System.currentTimeMillis();
        }

        // A teleport event usually cancels a run, so configure the teleport listener to ignore the next teleport event
        RunCancelListener.ignoreNextTeleport();
        Checkpoint.teleport(player, course, checkpoint);
    }

    public void sendResumeTipMessage() {
        String commandText;
        if (hasReachedCheckpoint()) {
            commandText = "/parkour resume " + course.getName();
        } else {
            commandText = "/parkour play " + course.getName();
        }

        ComponentBuilder message = new ComponentBuilder("Do ").color(ChatColor.RED)
                .append(commandText).color(ChatColor.AQUA)
                .event(new ClickEvent(ClickEvent.Action.RUN_COMMAND, commandText))
                .append(" to return to your run").color(ChatColor.RED)
                .event((ClickEvent) null);

        player.spigot().sendMessage(message.create());
    }

    public void remove(boolean fadeOut) {
        alive = false;
        ParkourManagerPlugin.getInstance().getRuns().remove(player);
        BossBarPool.removeAfter(player, bossBar, fadeOut ? 50 : 0);

        // If not fading out the parkour HUD, immediately remove the time message
        if (!fadeOut) {
            player.spigot().sendMessage(ChatMessageType.ACTION_BAR, new TextComponent(""));
        }

        Bukkit.getScheduler().cancelTask(speedChangePreventionTask);
        Bukkit.getScheduler().cancelTask(elytraPreventionTask);
        Bukkit.getScheduler().cancelTask(ridingPreventionTask);
        setPartialRun();

        Bukkit.getPluginManager().callEvent(
                GameLeftEvent.builder()
                        .minigame(() -> "parkour")
                        .player(player)
                        .build()
        );
    }

    private void setPartialRun() {
        int maximumCheckpointIndex = completedCheckpoints.stream()
                .mapToInt(course.getCheckpoints()::indexOf)
                .max().orElseThrow();

        long duration = System.currentTimeMillis() - startTime;

        if (maximumCheckpointIndex == 0 || maximumCheckpointIndex == course.getCheckpoints().size() - 1) {
            return;
        }

        new PartialRun(player, course, maximumCheckpointIndex, duration).save().whenComplete((nothing, error) -> {
            if (error != null) {
                ParkourManagerPlugin.getLog().log(Level.SEVERE, "Failed to set partial run", error);
            }
        });
    }

    public static Run resume(Player player, Course course, int checkpointIndex, long duration) {
        ComponentBuilder message = new ComponentBuilder("Resuming " + course.getName()).color(ChatColor.GREEN);
        player.spigot().sendMessage(message.create());

        Run run = new Run(player, course, System.currentTimeMillis() - duration);
        try {
            for (int completedCheckpoint = 0; completedCheckpoint <= checkpointIndex; completedCheckpoint++) {
                Checkpoint checkpoint = course.getCheckpoints().get(completedCheckpoint);
                run.completedCheckpoints.add(checkpoint);
            }
        } catch (IndexOutOfBoundsException e) {
            ParkourManagerPlugin.getLog().warning("Attempted to resume course " + course.getName() + " from " +
                    checkpointIndex + " which was out of bounds. You may need to clear partials");
        }

        sendTimeUpdate(run);
        run.bossBar.setProgress(checkpointIndex / ((float) course.getCheckpoints().size() - 1));

        ParkourManagerPlugin.getInstance().getRuns().put(player, run);
        fireGameStartedEvent(player);

        return run;
    }

    // Returns whether a run was actually cancelled
    public static boolean cancel(Player player) {
        Run runNullable = getOrNull(player);
        if (runNullable == null) {
            return false;
        } else {
            runNullable.remove(false);
            return true;
        }
    }

    // 0m 00s 000ms duration string
    public static String getLongDurationString(long millis) {
        long minutes = millis / (60 * 1000);
        long seconds = (millis / 1000) % 60;
        millis = millis % 1000;

        return String.format(Locale.ENGLISH, "%dm %02ds %03dms", minutes, seconds, millis);
    }

    // 0:00 duration string
    public static String getShortDurationString(long millis) {
        long minutes = millis / (60 * 1000);
        long seconds = (millis / 1000) % 60;

        return String.format(Locale.ENGLISH, "%d:%02d", minutes, seconds);
    }

    public static Run getOrNull(Player player) {
        Map<Player, Run> runs = ParkourManagerPlugin.getInstance().getRuns();
        return runs.get(player);
    }

    public static Run getOrStart(Player player, Course course) {
        Map<Player, Run> runs = ParkourManagerPlugin.getInstance().getRuns();
        return runs.computeIfAbsent(player, ignored -> startRun(player, course));
    }

    private static Run startRun(Player player, Course course) {
        ComponentBuilder message = new ComponentBuilder("Starting " + course.getName()).color(ChatColor.GREEN);
        player.spigot().sendMessage(message.create());

        Run run = new Run(player, course, System.currentTimeMillis());
        sendTimeUpdate(run);

        PartialRun previousRun = PartialRun.getOrNull(player, course);
        if (previousRun != null) {
            ComponentBuilder previousRunMessage = new ComponentBuilder("You have existing progress,")
                    .color(ChatColor.AQUA);

            previousRunMessage.append(" click here ")
                    .color(ChatColor.GREEN)
                    .event(new ClickEvent(
                            ClickEvent.Action.RUN_COMMAND,
                            "/parkour resume " + course.getName()
                    ));

            previousRunMessage.append("to resume")
                    .color(ChatColor.AQUA)
                    .event((ClickEvent) null);

            player.spigot().sendMessage(previousRunMessage.create());
        }

        fireGameStartedEvent(player);
        return run;
    }

    // Periodically send time messages to the player while the run is alive
    private static void sendTimeUpdate(Run run) {
        if (run.alive) {
            long duration = System.currentTimeMillis() - run.startTime;

            ComponentBuilder timeMessage = new ComponentBuilder(getShortDurationString(duration))
                    .color(ChatColor.GREEN)
                    .bold(true);

            run.player.spigot().sendMessage(ChatMessageType.ACTION_BAR, timeMessage.create());

            Bukkit.getScheduler().scheduleSyncDelayedTask(ParkourManagerPlugin.getInstance(), () -> sendTimeUpdate(run), 6);
        }
    }

    private static void fireGameStartedEvent(Player player) {
        Bukkit.getPluginManager().callEvent(
                GameStartedEvent.builder()
                        .minigame(() -> "parkour")
                        .player(player)
                        .item(
                                ClickableItem.builder()
                                        .material(Material.DIAMOND)
                                        .actionId(ItemClickListener.RETRY_ID)
                                        .name(ChatColor.AQUA + "Restart course")
                                        .build()
                        )
                        .item(
                                ClickableItem.builder()
                                        .material(Material.EMERALD)
                                        .actionId(ItemClickListener.LAST_CHECKPOINT_ID)
                                        .name(ChatColor.GREEN + "Go to last checkpoint")
                                        .build()
                        )
                        .build()
        );
    }
}
