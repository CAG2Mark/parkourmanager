package net.cubekrowd.parkourmanager;

import lombok.AllArgsConstructor;
import lombok.Value;
import org.bukkit.entity.Player;

@Value
@AllArgsConstructor
public class PlayerAndCourse {

    String playerUuid;
    String courseName;

    public PlayerAndCourse(Player player, Course course) {
        playerUuid = player.getUniqueId().toString();
        courseName = course.getName();
    }
}
