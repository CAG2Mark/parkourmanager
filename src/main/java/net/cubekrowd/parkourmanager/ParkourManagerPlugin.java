package net.cubekrowd.parkourmanager;

import com.mongodb.ConnectionString;
import com.mongodb.MongoClientSettings;
import com.mongodb.reactivestreams.client.MongoClient;
import com.mongodb.reactivestreams.client.MongoClients;
import com.mongodb.reactivestreams.client.MongoCollection;
import com.mongodb.reactivestreams.client.MongoDatabase;
import io.github.llewvallis.commandbuilder.AutoCommandBuilder;
import io.github.llewvallis.commandbuilder.DefaultInferenceProvider;
import lombok.Getter;
import net.cubekrowd.parkourmanager.commands.CourseArgument;
import net.cubekrowd.parkourmanager.commands.WorldArgument;
import org.bson.codecs.configuration.CodecRegistries;
import org.bson.codecs.configuration.CodecRegistry;
import org.bson.codecs.pojo.PojoCodecProvider;
import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ParkourManagerPlugin extends JavaPlugin {

    @Getter
    private static ParkourManagerPlugin instance;

    private MongoDatabase database;
    private String mongoCollectionPrefix;

    @Getter
    private Set<Course> courses;
    @Getter
    private Map<Player, Run> runs = new HashMap<>();
    @Getter
    private Map<PlayerAndCourse, PartialRun> partialRuns = new HashMap<>();
    @Getter
    private Set<Leaderboard> leaderboards = new HashSet<>();

    private String serverId;
    private boolean saveRecords;

    public PlayerNameLookup playerNameLookup;

    @Override
    public void onEnable() {
        playerNameLookup = new PlayerNameLookup();

        instance = this;
        saveDefaultConfig();

        FileConfiguration config = getConfig();
        String mongoAddress = config.getString("mongodb.uri");
        String mongoDatabase = config.getString("mongodb.database");
        mongoCollectionPrefix = config.getString("mongodb.collection-prefix");
        serverId = config.getString("server-id");
        saveRecords = config.getBoolean("save-records");

        int leaderboardRefreshRate = config.getInt("leaderboard-refresh-rate");
        int podiumRefreshRate = config.getInt("podium-refresh-rate");

        if (serverId == null) {
            getLog().log(Level.SEVERE, "server-id must be changed to something other than null in the config");
            this.setEnabled(false);
            return;
        }

        if (SimplePetsWrapper.isEnabled()) {
            registerListener(new SimplePetsWrapper());
        }

        CodecRegistry codecRegistry = CodecRegistries.fromRegistries(
                MongoClientSettings.getDefaultCodecRegistry(),
                CodecRegistries.fromProviders(PojoCodecProvider.builder().automatic(true).build())
        );

        MongoClientSettings dbClientSettings = MongoClientSettings.builder()
                .codecRegistry(codecRegistry)
                .applyConnectionString(new ConnectionString(mongoAddress))
                .build();

        MongoClient dbClient = MongoClients.create(dbClientSettings);

        database = dbClient.getDatabase(mongoDatabase);
        reloadData();

        registerCommands();

        registerListener(new CheckpointListener());
        registerListener(new RunCancelListener());
        registerListener(new ItemClickListener());

        Bukkit.getScheduler().scheduleSyncRepeatingTask(this, () -> leaderboards.forEach(Leaderboard::render), 0, leaderboardRefreshRate);
        Bukkit.getScheduler().scheduleSyncRepeatingTask(this, CitizensWrapper::refreshPodiums, 0, podiumRefreshRate);
        Bukkit.getScheduler().scheduleSyncRepeatingTask(this, playerNameLookup::fetchUnknownNames, 20, 10 * 20);
    }

    private void registerCommands() {
        DefaultInferenceProvider.getGlobal()
                .register(World.class, new WorldArgument())
                .register(Course.class, new CourseArgument());

        new AutoCommandBuilder(this)
                .jarSource(getFile(), "^net.cubekrowd.parkourmanager.commands.")
                .register();
    }

    @Override
    public void onDisable() {
        BossBarPool.removeAll();
        CitizensWrapper.removeAllPodiums();

        playerNameLookup = null;
    }

    public <T> MongoCollection<T> getMongoCollection(String name, Class<T> docClass) {
        return database.getCollection(mongoCollectionPrefix + name, docClass);
    }

    private void registerListener(Listener listener) {
        Bukkit.getPluginManager().registerEvents(listener, this);
    }

    public void reloadData() {
        courses = Course.loadFromDatabase();
        partialRuns = PartialRun.loadFromDatabase();
        leaderboards = Leaderboard.loadFromDatabase();
    }

    public static Logger getLog() {
        return instance.getLogger();
    }

    public static String getServerId() {
        return instance.serverId;
    }

    public static boolean shouldSaveRecords() {
        return instance.saveRecords;
    }
}
