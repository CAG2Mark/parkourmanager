package net.cubekrowd.parkourmanager;

import lombok.AllArgsConstructor;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class CheckpointListener implements Listener {

    private static Map<OfflineLocation, CourseAndCheckpoint> checkpointLocs = null;

    @AllArgsConstructor
    private static class CourseAndCheckpoint {
        private Course course;
        private Checkpoint checkpoint;
    }

    // Listening for PlayerInteractEvent doesn't work as well since it is only guaranteed to be fired once when the
    // player steps onto the pressure plate (although in practice it is sometimes called unpredictably while the player
    // is standing on the pressure plate).
    @EventHandler
    public void onPlayerMove(PlayerMoveEvent e) {
        Player player = e.getPlayer();

        // In practice this value is slightly too large, so we'll trim it to make it more consistent
        double playerRadius = player.getBoundingBox().getWidthX() / 2 - 0.01;

        // Blocks the player was standing in
        Set<Location> previouslyOccupiedBlocks = getOccupiedBlocks(e.getFrom(), playerRadius);
        // Blocks the player is standing in
        Set<Location> currentlyOccupiedBlocks = getOccupiedBlocks(e.getTo(), playerRadius);

        // Blocks the player is walking out of
        Set<Location> oldBlocks = new HashSet<>(previouslyOccupiedBlocks);
        oldBlocks.removeAll(currentlyOccupiedBlocks);

        // Blocks the player is walking into
        Set<Location> newBlocks = new HashSet<>(currentlyOccupiedBlocks);
        newBlocks.removeAll(previouslyOccupiedBlocks);

        buildCheckpointLocationsIfNecessary();

        // Detection for leaving the first checkpoint
        var copyLoc = new OfflineLocation();

        for (Location oldBlock : oldBlocks) {
            copyLoc.copyLocation(oldBlock);
            CourseAndCheckpoint entry = checkpointLocs.get(copyLoc);
            if (entry == null) {
                continue;
            }

            Course course = entry.course;
            Checkpoint checkpoint = entry.checkpoint;

            boolean isFirstCheckpoint = course.getCheckpoints().indexOf(checkpoint) == 0;
            if (isFirstCheckpoint) {
                onCheckpoint(player, course, checkpoint);
            }
        }

        // Detection for entering a subsequent checkpoint
        for (Location newBlock : newBlocks) {
            copyLoc.copyLocation(newBlock);
            CourseAndCheckpoint entry = checkpointLocs.get(copyLoc);
            if (entry == null) {
                continue;
            }

            Course course = entry.course;
            Checkpoint checkpoint = entry.checkpoint;

            boolean isFirstCheckpoint = course.getCheckpoints().indexOf(checkpoint) == 0;
            if (!isFirstCheckpoint) {
                onCheckpoint(player, course, checkpoint);
            }
        }
    }

    private Set<Location> getOccupiedBlocks(Location location, double playerRadius) {
        Set<Location> result = new HashSet<>();

        result.add(location.clone().add(playerRadius, 0, playerRadius).getBlock().getLocation());
        result.add(location.clone().add(playerRadius, 0, -playerRadius).getBlock().getLocation());
        result.add(location.clone().add(-playerRadius, 0, playerRadius).getBlock().getLocation());
        result.add(location.clone().add(-playerRadius, 0, -playerRadius).getBlock().getLocation());

        return result;
    }

    private void onCheckpoint(Player player, Course course, Checkpoint checkpoint) {
        if (SimplePetsWrapper.isPlayerRidingPet(player) || player.isFlying()) {
            return;
        }

        // Starting a course that only has a single checkpoint can cause issues
        if (course.getCheckpoints().size() < 2) {
            return;
        }

        int index = course.getCheckpoints().indexOf(checkpoint);

        Run runNullable = Run.getOrNull(player);
        if (runNullable == null) {
            if (index == 0) {
                // This won't start a course if a run is already active
                Run.getOrStart(player, course);
            }
        } else {
            if (index == 0 && runNullable.hasReachedCheckpoint()) {
                Run.cancel(player);
                runNullable = Run.getOrStart(player, course);
            }

            // Only activate the checkpoint if its in the current course
            if (runNullable.getCourse().getCheckpoints().contains(checkpoint)) {
                runNullable.onCheckpoint(checkpoint);
            }
        }
    }

    private void buildCheckpointLocationsIfNecessary() {
        if (checkpointLocs == null) {
            checkpointLocs = new HashMap<>();

            for (Course course : ParkourManagerPlugin.getInstance().getCourses()) {
                for (Checkpoint checkpoint : course.getCheckpoints()) {
                    var location = new OfflineLocation(course.getWorldName(), checkpoint.getX(), checkpoint.getY(), checkpoint.getZ());
                    CourseAndCheckpoint entry = new CourseAndCheckpoint(course, checkpoint);

                    checkpointLocs.put(location, entry);
                }
            }
        }
    }

    public static void invalidateCheckpointLocations() {
        checkpointLocs = null;
    }
}
