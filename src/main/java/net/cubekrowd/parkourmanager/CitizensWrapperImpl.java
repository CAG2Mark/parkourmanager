package net.cubekrowd.parkourmanager;

import net.citizensnpcs.api.CitizensAPI;
import net.citizensnpcs.api.npc.MemoryNPCDataStore;
import net.citizensnpcs.api.npc.NPC;
import net.citizensnpcs.api.npc.NPCRegistry;
import net.citizensnpcs.api.trait.Trait;
import org.bson.types.ObjectId;
import org.bukkit.Location;
import org.bukkit.entity.EntityType;
import org.bukkit.event.player.PlayerTeleportEvent;
import org.joor.Reflect;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.logging.Level;

public class CitizensWrapperImpl {

    private NPCRegistry registry = CitizensAPI.createAnonymousNPCRegistry(new MemoryNPCDataStore());
    private Map<ObjectId, NPC> trackedPodiums = new HashMap<>();

    public void render(ObjectId key, Location location, String name) {
        trackedPodiums.compute(key, (_key, npc) -> {
            // @NOTE(traks) destroy and respawn NPCs when their name changes,
            // because changing names is currently bugged (MC 1.16.2). It leaves
            // the NPC's name in the tab list if a real player with the same
            // name is online. Could also be an issue with BungeeTabListPlus,
            // who knows?!

            if (npc != null) {
                if (npc.getName().equals(name)) {
                    ParkourManagerPlugin.getLog().info("Skipped rendering of podium " + name + " since its name did not change");
                    npc.teleport(location, PlayerTeleportEvent.TeleportCause.PLUGIN);
                    return npc;
                } else {
                    ParkourManagerPlugin.getLog().info("Rerendering podium " + npc.getName() + " since its name changed to " + name);
                    npc.destroy();
                }
            } else {
                ParkourManagerPlugin.getLog().info("Rendering podium " + name + " for the first time");
            }

            npc = registry.createNPC(EntityType.PLAYER, name);

            try {
                npc.data().set(NPC.Metadata.NAMEPLATE_VISIBLE, false);

                @SuppressWarnings("unchecked")
                Class<? extends Trait> gravityClass = (Class<? extends Trait>) Class.forName("net.citizensnpcs.trait.Gravity");
                Reflect.on(npc.getTrait(gravityClass)).call("toggle");

                @SuppressWarnings("unchecked")
                Class<? extends Trait> lookCloseClass = (Class<? extends Trait>) Class.forName("net.citizensnpcs.trait.LookClose");
                Reflect lookCloseTrait = Reflect.on(npc.getTrait(lookCloseClass));
                lookCloseTrait.call("setRandomLook", true);
                lookCloseTrait.call("toggle");
            } catch (Exception e) {
                ParkourManagerPlugin.getLog().log(Level.SEVERE, "Failed to configure podium", e);
            }

            npc.spawn(location);

            return npc;
        });
    }

    public void removeIfExists(ObjectId key) {
        NPC npc = trackedPodiums.remove(key);
        if (npc == null) {
            return;
        }

        ParkourManagerPlugin.getLog().info("Removing podium " + npc.getName());
        npc.destroy();
    }

    public void removeAll() {
        new HashSet<>(trackedPodiums.keySet()).forEach(this::removeIfExists);
    }

    public void refreshPodiums() {
        trackedPodiums.values().forEach(npc -> {
            if (npc.isSpawned()) {
                Reflect.on(npc.getEntity())
                        .call("getSkinTracker")
                        .call("updateNearbyViewers", 128.0);
            }
        });
    }
}
