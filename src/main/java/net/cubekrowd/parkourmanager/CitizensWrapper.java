package net.cubekrowd.parkourmanager;

import org.bson.types.ObjectId;
import org.bukkit.Bukkit;
import org.bukkit.Location;

public class CitizensWrapper {

    private static CitizensWrapperImpl instance;

    public static CitizensWrapperImpl getInstance() {
        if (instance != null) {
            return instance;
        }

        if (Bukkit.getPluginManager().isPluginEnabled("Citizens")) {
            instance = new CitizensWrapperImpl();
            return instance;
        }

        return null;
    }

    public static void renderPodium(ObjectId key, Location location, String name) {
        if (getInstance() != null) {
            getInstance().render(key, location, name);
        }
    }

    public static void removePodiumIfExists(ObjectId key) {
        if (getInstance() != null) {
            getInstance().removeIfExists(key);
        }
    }

    public static void removeAllPodiums() {
        if (getInstance() != null) {
            getInstance().removeAll();
        }
    }

    public static void refreshPodiums() {
        if (getInstance() != null) {
            getInstance().refreshPodiums();
        }
    }
}
