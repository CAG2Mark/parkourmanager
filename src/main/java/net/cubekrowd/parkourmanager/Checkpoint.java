package net.cubekrowd.parkourmanager;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Entity;
import org.bukkit.util.Vector;

import java.util.Locale;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Checkpoint {

    private int x;
    private int y;
    private int z;

    @EqualsAndHashCode.Exclude
    private double xOffset;
    @EqualsAndHashCode.Exclude
    private double yOffset;
    @EqualsAndHashCode.Exclude
    private double zOffset;

    @EqualsAndHashCode.Exclude
    private int yaw;

    public static void teleport(Entity entity, Course course, Checkpoint checkpoint) {
        var world = Bukkit.getWorld(course.getWorldName());
        if (world == null) {
            return;
        }

        Location location = new Location(
                world,
                checkpoint.x + checkpoint.xOffset,
                checkpoint.y + checkpoint.yOffset,
                checkpoint.z + checkpoint.zOffset,
                checkpoint.yaw, 0
        );

        entity.teleport(location);
        entity.setVelocity(new Vector());
    }

    public String toDisplayString() {
        return String.format(Locale.ENGLISH, "%s, %s, %s, [%+.3f, %+.3f, %+.3f *%s]", x, y, z, xOffset, yOffset, zOffset, yaw);
    }
}
